// add doc ready functions
//document.addEventListener("DOMContentLoaded", makeRadiosDeselectable, false);

function _radioDeselectHandler(e) {
	var elem = e.target;
	var checkedAttrVal = elem.getAttribute("checked");
	elem.checked = (checkedAttrVal === "false");
	// fire changed event
	var groupElem = getAnyParentWithTag(elem, "tr")
	var radiosInGroup = groupElem.querySelectorAll("input[type=radio]");
	radiosInGroup.forEach(function(e2) {
		e2.setAttribute("checked", e2.checked);
	});
}

function makeRadiosDeselectable(lState) {
	// make radio buttons deselectable
	var radioButtons = document.querySelectorAll("input[type=radio]");
	radioButtons.forEach(function(elem) {
		if (lState === true) {
			elem.setAttribute("checked", elem.checked);
			elem.addEventListener("click", _radioDeselectHandler, false);
		} else {
			elem.removeEventListener("click", _radioDeselectHandler, false);
		}
	});
}

function setupAutoCalcOnInputs(calcHandler, lState) {
	var userInputs = document.querySelectorAll("input.user-input");
	userInputs.forEach(function(elem) {
		if (lState === true) {
			elem.addEventListener("change", calcHandler, false);
		} else {
			elem.removeEventListener("change", calcHandler, false);
		}
	});
}

function saveToFile(filename, data) {
	var blob = new Blob([data], {type: "text/plain"});
	if (window.navigator.msSaveOrOpenBlob) {
		window.navigator.msSaveBlob(blob, filename);
	} else {
		var elem = window.document.createElement("a");
		elem.href = window.URL.createObjectURL(blob);
		elem.download = filename;
		document.body.appendChild(elem);
		elem.click();
		document.body.removeChild(elem);
	}
}