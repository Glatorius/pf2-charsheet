// constants
var infoDb = {

	abilities: [
		{id:"str", name:"Strength"},
		{id:"dex", name:"Dexterity"},
		{id:"con", name:"Constitution"},
		{id:"int", name:"Intelligence"},
		{id:"wis", name:"Wisdom"},
		{id:"cha", name:"Charisma"}
	],

	proficiencies: [
		{id:"u", name:"Untrained", desc:"0", level:0, bonus:0},
		{id:"t", name:"Trained", desc:"Level + 2", level:1, bonus:2},
		{id:"e", name:"Expert", desc:"Level + 4", level:1, bonus:4},
		{id:"m", name:"Master", desc:"Level + 6", level:1, bonus:6},
		{id:"l", name:"Legendary", desc:"Level + 8", level:1, bonus:8}
		//{id:"i1", name:"Improvised Half", level:0.5, bonus:0},
		//{id:"i7", name:"Improvised Full", level:1, bonus:0}
	],

	defaultProficiencyId: "u",

	skills: [
		{id:"acr", name:"Acrobatics", ability:"dex", armor:true, hasSub:false},
		{id:"arc", name:"Arcana", ability:"int", armor:false, hasSub:false},
		{id:"ath", name:"Athletics", ability:"str", armor:true, hasSub:false},
		{id:"cra", name:"Crafting", ability:"int", armor:false, hasSub:false},
		{id:"dec", name:"Deception", ability:"cha", armor:false, hasSub:false},
		{id:"dip", name:"Diplomacy", ability:"cha", armor:false, hasSub:false},
		{id:"int", name:"Intimidation", ability:"cha", armor:false, hasSub:false},
		{id:"lor", name:"Lore", ability:"int", armor:false, hasSub:true},
		{id:"med", name:"Medicine", ability:"wis", armor:false, hasSub:false},
		{id:"nat", name:"Nature", ability:"wis", armor:false, hasSub:false},
		{id:"occ", name:"Occultism", ability:"int", armor:false, hasSub:false},
		{id:"per", name:"Performance", ability:"cha", armor:false, hasSub:false},
		{id:"rel", name:"Religion", ability:"wis", armor:false, hasSub:false},
		{id:"soc", name:"Society", ability:"int", armor:false, hasSub:false},
		{id:"ste", name:"Stealth", ability:"dex", armor:true, hasSub:false},
		{id:"sur", name:"Survival", ability:"wis", armor:false, hasSub:false},
		{id:"thi", name:"Thievery", ability:"dex", armor:false, hasSub:false},
	],

	armorClasses: [
		{id:"ac", name:"AC", hasSub:false},
		{id:"st", name:"Stance", hasSub:true}
	],
	
	saves: [
		{id:"fort", name:"Fortitude", ability:"con"},
		{id:"refl", name:"Reflex", ability:"dex"},
		{id:"will", name:"Will", ability:"wil"},
	],
	
}