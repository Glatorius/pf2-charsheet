// data
var sections = [];
var _charData = {};
var _subSeperator = "_";

document.addEventListener("DOMContentLoaded", initToolbar, false);
document.addEventListener("DOMContentLoaded", initForm, false);

function initToolbar() {
		var lockBtn = document.querySelectorAll("#lock_sheet")[0];
		if (lockBtn) {
			lockBtn.addEventListener("change", doAutoCalcViaForm, false);
		}
		var newBtn = document.querySelectorAll("#new_data")[0];
		if (newBtn) {
			newBtn.addEventListener("click", newData, false);
		}
		var loadBtn = document.querySelectorAll("#load_data")[0];
		if (loadBtn) {
			loadBtn.addEventListener("change", loadData, false);
		}
		var saveBtn = document.querySelectorAll("#save_data")[0];
		if (saveBtn) {
			saveBtn.addEventListener("click", saveData, false);
		}
}

function initForm() {
		var formElem = document.querySelectorAll("form")[0];
		if (formElem) {
			_charData = loadOrCreateCharData(true);
			doAutoCalc(_charData);
			saveCharData();
			buildForm(_charData, formElem);
		}
}

function doAutoCalcViaForm(event) {
	var formElem = getAnyParentWithTag(event.target, "form");
	if (!formElem) {
		formElem = document;
	}
	_charData = createEmptyCharData(false);
	serializeForm(formElem, _charData, false);
	doAutoCalc(_charData);
	saveCharData();
	buildForm(_charData, formElem);
}

function loadOrCreateCharData(defaultScores) {
	var charData = null;
	if (window.localStorage.currentCharacterData) {
		charData = JSON.parse(window.localStorage.currentCharacterData);
	}
	if (!charData || !charData.level) {
		charData = createEmptyCharData(defaultScores);
	}
	return charData;
}

function saveCharData() {
	window.localStorage.currentCharacterData = JSON.stringify(_charData);
}

function createEmptyCharData(defaultScores) {	
	var charData = {};
	charData.level = 1;
	charData.abilities = [];
	charData.proficiencies = [];
	charData.skills = [];
	
	if (defaultScores === true) {
		charData.abilities.push({"id":"str", "score":10});
		charData.abilities.push({"id":"dex", "score":10});
		charData.abilities.push({"id":"con", "score":10});
		charData.abilities.push({"id":"int", "score":10});
		charData.abilities.push({"id":"wis", "score":10});
		charData.abilities.push({"id":"cha", "score":10});
	}
	
	return charData;
}

function serializeForm(formElem, data, allInputs) {
		
	// sections
	sections.forEach(function(section) {section.serialize(formElem, data)});

}

function doAutoCalc(data) {
		
	// sections
	sections.forEach(function(section) {section.calculate(data)});
	
}

function buildForm(data, formElem) {
	setupAutoCalcOnInputs(doAutoCalcViaForm, false);
	makeRadiosDeselectable(false);
	var isLocked = getInputValue(document, "lock_sheet");	
	
	// sections
	sections.forEach(function(section) {section.buildForm(data, formElem, isLocked)});
	
	// controls visibility
	var controls = document.querySelectorAll(".controls");
	setAllHidden(controls, isLocked);
	
	fillProficiencyContainers(formElem);
	makeRadiosDeselectable(true);
	setupAutoCalcOnInputs(doAutoCalcViaForm, true);
}

function getAnyParentWithTag(elem, tag) {
    var parent;
    // traverse parents
    while (elem) {
        parent = elem.parentElement;
        if (parent && parent.tagName.toUpperCase() === tag.toUpperCase()) {
            return parent;
        }
        elem = parent;
    }
    return null;
}

function setClass(target, className, unset) {
	if (unset === true) {
		target.classList.remove(className);
	} else {
		target.classList.add(className);
	}
}

function setAllHidden(targets, condition, checkContent, content) {
	var isHidden = condition && (!checkContent || (content == null) || (content.length === 0));
	targets.forEach(function(elem) {
		setClass(elem, "hidden", !isHidden);
	});
}

function setHidden(parentElem, id, condition, checkContent, content) {
	var elemArray = parentElem.querySelectorAll("#"+id);
	if (elemArray.length > 0) {
		setAllHidden(elemArray, condition, checkContent, content);
	}
}

function getInputValue(parent, id) {
	var inputArray = parent.querySelectorAll("input#"+id);
	if (inputArray.length > 0) {
		if ((inputArray[0].type === "checkbox") || (inputArray[0].type === "radio")) {
			return inputArray[0].checked;
		} else {
			return inputArray[0].value;
		}
	}
	return undefined;
}

function getProfValue(parent, baseId) {
	var prof = infoDb.defaultProficiencyId;
	infoDb.proficiencies.forEach(function(elem) {
		if (elem.id != "u") {
			var inputArray = parent.querySelectorAll("input#"+baseId+"_"+elem.id);
			if (inputArray.length > 0) {
				if (inputArray[0].checked) {
					prof = elem.id;
				}
			}
		}
	});
	return prof;
}

function setInputValue(parent, id, value) {
	var textVal = (value == null) ? "" : value;
	var inputArray = parent.querySelectorAll("input#"+id);
	if (inputArray.length > 0) {
		if ((inputArray[0].type === "checkbox") || (inputArray[0].type === "radio")) {
			inputArray[0].checked = value;
		} else {
			inputArray[0].value = textVal;
		}
	}
}

function setProfValue(parent, baseId, value) {
	var allGroupInputs = parent.querySelectorAll("input[id^='"+baseId+"'");
	allGroupInputs.forEach(function(inp) {
		inp.checked = false;
	});
	var inputArray = parent.querySelectorAll("input#"+baseId+"_"+value);
	if (inputArray.length > 0) {
		inputArray[0].checked = true;
	}
}

function setupProficiencyContainer(parent, id, value, forCalc) {
	var contArray = parent.querySelectorAll("#"+id+".prof-block-container");
	if (contArray.length > 0) {
		if (value) {
			contArray[0].setAttribute("data-prof-val", value);
		}
		if (forCalc) {
			contArray[0].setAttribute("data-prof-calc", forCalc);
		}
	}
}

function fillProficiencyContainers(parentElem) {
	var profBlockTemp = parentElem.querySelectorAll("#template_profblock")[0];
	var allContainers = parentElem.querySelectorAll(".prof-block-container");
	allContainers.forEach(function(cont) {
		cont.innerHTML = "";
		var id = cont.id;
		var val = cont.getAttribute("data-prof-val");
		var calc = cont.getAttribute("data-prof-calc");
		var replacements = [
			[/{id}/g, id],
			[/{calc-class}/g, (calc) ? "prof-calc" : "prof-free"],
			[/{input-class}/g, (calc) ? "user-input" : ""],
		];
		var profValues = [
			[id, (val) ? val : infoDb.defaultProficiencyId]
		];
		addTemplateToTarget(profBlockTemp, cont, replacements, [], profValues);
	});
	
}

function addTemplateToTarget(template, target, replacements, textValues, profValues, callback) {
	var clon = template.cloneNode(true);

	var htm = clon.innerHTML;
	if (replacements) {
		replacements.forEach(function(repl) {
			htm = htm.replace(repl[0], repl[1]);
		});
	}
	clon.innerHTML = htm;
	
	clon.content.childNodes.forEach(function(child) {
		if (child.nodeType === 1) {
			if (textValues) {
				textValues.forEach(function(tInput) {
					setInputValue(child, tInput[0], tInput[1]);
				});
			}
			
			if (profValues) {
				profValues.forEach(function(pInput) {
					setupProficiencyContainer(child, pInput[0], pInput[1], pInput[2]);
					setProfValue(child, pInput[0], pInput[1]);
				});
			}
		}
	});
	target.appendChild(clon.content);
}

function newData(event) {
	window.localStorage.currentCharacterData = null;
	initForm();
}

function loadData(event) {
	var input = event.target;
	var file = input.files[0];
	var fr = new FileReader();
	fr.onload = function() {
		var fr = this;
		var dataText = fr.result;
		window.localStorage.currentCharacterData = dataText;
		initForm();
	}
	fr.readAsText(file);
	//window.localStorage.currentCharacterData = null;
	//initForm();
}

function saveData(event) {
	var name = "newcharacter";
	if (_charData && _charData.characterName && _charData.characterName.length > 0) {
		name = _charData.characterName;
	}
	saveToFile(name+".json", JSON.stringify(_charData));
}