var sectionHead = {
	
	sectionHtml: "",
	
	templateHtml: "",
	
	serialize: function(formElem, data) {
		data.characterName = getInputValue(formElem, "character_name");
		data.playerName = getInputValue(formElem, "player_name");
		data.size = getInputValue(formElem, "size");
		data.alignment = getInputValue(formElem, "alignment");
		data.traits = getInputValue(formElem, "traits");
		data.deity = getInputValue(formElem, "deity");
		data.ancestry = getInputValue(formElem, "ancestry");
		data.background = getInputValue(formElem, "background");
		data.className = getInputValue(formElem, "class");
		data.experience = getInputValue(formElem, "experience");
		
	}, // serialize
	
	calculate: function(data) {
	}, // calculate
	
	buildForm: function(data, formElem, isLocked) {
		setInputValue(formElem, "character_name", data.characterName);
		setInputValue(formElem, "player_name", data.playerName);
		setInputValue(formElem, "size", data.size);
		setInputValue(formElem, "alignment", data.alignment);
		setInputValue(formElem, "traits", data.traits);
		setInputValue(formElem, "deity", data.deity);
		setInputValue(formElem, "ancestry", data.ancestry);
		setInputValue(formElem, "background", data.background);
		setInputValue(formElem, "class", data.className);
		setInputValue(formElem, "experience", data.experience);
	}, // buildForm
	
}

sections.push(sectionHead);