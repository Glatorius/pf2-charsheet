var sectionDefense = {
	
	sectionHtml: "",
	
	templateHtml: "",
	
	serialize: function(formElem, data) {
		// armor classes
		data.armorClasses = [];
		var allAcEntries = formElem.querySelectorAll("input[id^='ac_sub_'");
		var subIds = [];
		allAcEntries.forEach(function(elem) {
			var split = elem.getAttribute("id").substr(7).split(_subSeperator);
			var acId = split[0];
			var acSub = (split.length > 1) ? split[1] : "";
			subIds.push({"id":acId, "sub":acSub});
		});
		infoDb.armorClasses.forEach(function(acInfo) {
			subIds.filter(function(si) {return si.id === acInfo.id}).forEach(function(si) {
				var sub = si.sub;
				var selector = (sub.length > 0) ? si.id+_subSeperator+sub : si.id;
				var prof = getProfValue(formElem, "ac_"+selector+"_prof")
				var cap = getInputValue(formElem, "ac_"+selector+"_cap");
				var item = getInputValue(formElem, "ac_"+selector+"_item");
				var status = getInputValue(formElem, "ac_"+selector+"_status");
				var circum = getInputValue(formElem, "ac_"+selector+"_circum");

				if ((acInfo.hasSub) && (si.sub.length === 0)) {
					var sub = getInputValue(formElem, "ac_sub_"+selector);
				}
				if ((sub.length > 0) || !(acInfo.hasSub)) {
					data.armorClasses.push({id:si.id, sub:sub, prof:prof, cap:cap, item:item, status:status, circum:circum});
				}
			});
		});
		
		// proficiencies
		data.profArmorUnarmored = getProfValue(formElem, "armor_unarmored_prof");
		data.profArmorLight = getProfValue(formElem, "armor_light_prof");
		data.profArmorMedium = getProfValue(formElem, "armor_medium_prof");
		data.profArmorHeavy = getProfValue(formElem, "armor_heavy_prof");
	}, // serialize
	
	calculate: function(data) {
		// armor classes
		if (!(data.armorClasses)) {
			data.armorClasses = [];
		}
		var ability = data.abilities.find(function(x) {return x.id == "dex";});
		data.armorClasses.forEach(function(elem) {
			var prof = data.proficiencies.find(function(x) {return x.id == elem.prof;});
			var cap = parseInt(elem.cap);
			var effectiveDex = (cap < ability.mod) ? cap : ability.mod;
			elem.value = 10 + effectiveDex + prof.value + parseInt(elem.item) + parseInt(elem.status) + parseInt(elem.circum);
		});
	}, // calculate
	
	buildForm: function(data, formElem, isLocked) {
		// armor classes
		var acEntryTemp = formElem.querySelectorAll("#template_acentry")[0];
		var acList = formElem.querySelectorAll("#ac_list")[0];
		acList.innerHTML = "";

		var acEntries = infoDb.armorClasses.reduce(function(accum, curr) {
			var acDatas = data.armorClasses.filter(function(x) {return x.id === curr.id;});
			if ((curr.hasSub && !isLocked) || (acDatas.length === 0 && !curr.hasSub)) {
				acDatas.push(null);
			}
			acDatas.forEach(function(x) {
				accum.push({"info":curr, "data":x});
			});
			return accum;
		}, []);
		
		acEntries.forEach(function(entry){
			var id = entry.info.id;
			var hasData = (entry.data != null);
			var sub = (hasData && entry.data.sub) ? entry.data.sub : "";
			var entrySubId = (sub) ? id+_subSeperator+sub : id;
			var replacements = [
				[/{id}/g, entrySubId],
				[/{name}/g, entry.info.name],
				[/{sub_class}/g, (entry.info.hasSub) ? "" : "hidden"],
			];
			var textValues = [
				["ac_sub_" + entrySubId, sub],
				["ac_" + entrySubId, (hasData) ? entry.data.value : ""],
				["ac_" + entrySubId + "_cap", (hasData) ? entry.data.cap : 0],
				["ac_" + entrySubId + "_item", (hasData) ? entry.data.item : 0],
				["ac_" + entrySubId + "_status", (hasData) ? entry.data.status : 0],
				["ac_" + entrySubId + "_circum", (hasData) ? entry.data.circum : 0],
			];
			var profValues = [
				["ac_" + entrySubId + "_prof", (hasData) ? entry.data.prof : infoDb.defaultProficiencyId, true]
			];
			addTemplateToTarget(acEntryTemp, acList, replacements, textValues, profValues);
		});		
		
		// proficiencies
		setupProficiencyContainer(formElem, "armor_unarmored_prof", data.profArmorUnarmored, false);
		setupProficiencyContainer(formElem, "armor_light_prof", data.profArmorLight, false);
		setupProficiencyContainer(formElem, "armor_medium_prof", data.profArmorMedium, false);
		setupProficiencyContainer(formElem, "armor_heavy_prof", data.profArmorHeavy, false);
	}, // buildForm
	
}

sections.push(sectionDefense);