var sectionSkills = {
	
	sectionHtml: "",
	
	templateHtml: "",
	
	serialize: function(formElem, data) {
		var allSkillEntries = formElem.querySelectorAll("input[id^='skill_sub_'");
		var subIds = [];
		allSkillEntries.forEach(function(elem) {
			var split = elem.getAttribute("id").substr(10).split(_subSeperator);
			var skillId = split[0];
			var skillSub = (split.length > 1) ? split[1] : "";
			subIds.push({"id":skillId, "sub":skillSub});
		});
		infoDb.skills.forEach(function(skillInfo) {
			subIds.filter(function(si) {return si.id === skillInfo.id}).forEach(function(si) {
				var sub = si.sub;
				var selector = (sub.length > 0) ? si.id+_subSeperator+sub : si.id;
				var prof = getProfValue(formElem, "skill_"+selector+"_prof_")
				var item = getInputValue(formElem, "skill_"+selector+"_item");
				var armor = getInputValue(formElem, "skill_"+selector+"_armor");
				var notes = getInputValue(formElem, "skill_"+selector+"_notes");

				if ((skillInfo.hasSub) && (si.sub.length === 0)) {
					var sub = getInputValue(formElem, "skill_sub_"+selector);
				}
				if ((sub.length > 0) || !(skillInfo.hasSub)) {
					data.skills.push({"id":si.id, "sub":sub, "prof":prof, "item":item, "armor":armor, "notes":notes});
				}
			});
		});
	}, // serialize
	
	calculate: function(data) {
		data.skills.forEach(function(elem) {
			var skillInfo = infoDb.skills.find(function(x) {return x.id == elem.id;});
			var ability = data.abilities.find(function(x) {return x.id == skillInfo.ability;});
			var prof = data.proficiencies.find(function(x) {return x.id == elem.prof;});
			elem.value = ability.mod + prof.value + parseInt(elem.item) - parseInt(elem.armor);
		});
	}, // calculate
	
	buildForm: function(data, formElem, isLocked) {
		var skillEntryTemp = formElem.querySelectorAll("#template_skillentry")[0];
		var skillHeaderTemp = formElem.querySelectorAll("#template_skillheader")[0];
		var skillList = formElem.querySelectorAll("#skill_list")[0];
		skillList.innerHTML = "";

		var groupProf = getInputValue(formElem, "skill_grouping_prof");
		var groupAbl = getInputValue(formElem, "skill_grouping_abl");
		
		var skillGroups = infoDb.skills.reduce(function(accum, curr) {
			var skillDatas = data.skills.filter(function(x) {return x.id === curr.id;});
			if ((curr.hasSub && !isLocked) || (skillDatas.length === 0 && !curr.hasSub)) {
				skillDatas.push(null);
			}
			skillDatas.forEach(function(x) {
				var key = (groupProf) ? ((x) ? x.prof : "u") : ((groupAbl) ? curr.ability : "");
				var groups = accum.filter(function(y) {return y.key === key});
				var group = null;
				if (groups.length > 0) {
					group = groups[0];
				} else {
					group = {"key":key, "entries":[]};
					accum.push(group);
				}
				group.entries.push({"skillInfo":curr, "skillData":x});
			});
			return accum;
		}, []);
		
		var orderList = (groupProf) ? infoDb.proficiencies.flat(0).reverse() : ((groupAbl) ? infoDb.abilities : [{"id":""}]);
		
		orderList.forEach(function(ord) {
			var groups = skillGroups.filter(function(x) {return x.key === ord.id});
			if (groups.length > 0) {
				var group = groups[0];
				if (group.key.length > 0) {
					var headerText = group.key;
					if (groupProf) {
						headerText = infoDb.proficiencies.find(function(x) {return x.id === group.key;}).name;
					} else if (groupAbl) {
						headerText = infoDb.abilities.find(function(x) {return x.id === group.key;}).name;
					}
					var headReplacements = [
						[/{header}/g, headerText],
					];
					addTemplateToTarget(skillHeaderTemp, skillList, headReplacements, null, null);
				}
				group.entries.forEach(function(entry){
					var id = entry.skillInfo.id;
					var subText = (entry.skillData) ? ((entry.skillData.sub) ? entry.skillData.sub : "") : "";
					var sub = (subText) ? _subSeperator+subText : "";
					var hasData = (entry.skillData != null);
					var replacements = [
						[/{id}/g, id],
						[/{name}/g, entry.skillInfo.name],
						[/{sub}/g, sub],
						[/{sub_class}/g, (entry.skillInfo.hasSub) ? "" : "hidden"],
						[/{abl}/g, entry.skillInfo.ability.toUpperCase()],
						[/{armor_class}/g, (entry.skillInfo.armor) ? "" : "hidden"],
						[/{notes_class}/g, (!isLocked || (entry.skillData && entry.skillData.notes.length > 0)) ? "" : "hidden"],
					];
					var textValues = [
						["skill_sub_" + id + sub, subText],
						["skill_" + id + sub + "_item", (hasData) ? entry.skillData.item : 0],
						["skill_" + id + sub + "_armor", (hasData) ? entry.skillData.armor : 0],
						["skill_" + id + sub + "_notes", (hasData) ? entry.skillData.notes : ""],
						["skill_" + id + sub, (hasData) ? entry.skillData.value : ""],
					];
					var profValues = [
						["skill_" + id + sub + "_prof_", (hasData) ? entry.skillData.prof : infoDb.defaultProficiencyId]
					];
					addTemplateToTarget(skillEntryTemp, skillList, replacements, textValues, profValues);
				});
			}
		});
	}, // buildForm
	
}

sections.push(sectionSkills);