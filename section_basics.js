var sectionBasics = {
	
	sectionHtml: "",
	
	templateHtml: "",
	
	serialize: function(formElem, data) {
		// level
		data.level = getInputValue(formElem, "level");
		
		// abilities
		infoDb.abilities.forEach(function(elem) {
			var score = getInputValue(formElem, "abl_"+elem.id+"_score");
			var mod = getInputValue(formElem, "abl_"+elem.id+"_mod");
			data.abilities.push({"id":elem.id, "score":score, "mod":mod});
		});
		data.abilityNotes = getInputValue(formElem, "ability_notes");
	}, // serialize
	
	calculate: function(data) {
		// abilities
		data.abilities.forEach(function(elem) {
			elem.mod = Math.floor((elem.score - 10)/2);
		});
		
		// proficiencies
		data.proficiencies = [];
		infoDb.proficiencies.forEach(function(elem) {
			var prof = {};
			prof.id = elem.id;
			prof.value = Math.floor(elem.level * data.level) + elem.bonus;
			data.proficiencies.push(prof);
		});
	}, // calculate
	
	buildForm: function(data, formElem, isLocked) {
		// level
		setInputValue(formElem, "level", data.level);
		
		// abilities
		var abilityEntryTemp = formElem.querySelectorAll("#template_abilityentry")[0];
		var abilityList = formElem.querySelectorAll("#ability_list")[0];
		abilityList.innerHTML = "";
		infoDb.abilities.forEach(function(elem) {
			var ablData = data.abilities.find(function(x) {return x.id == elem.id;});
			var replacements = [
				[/{id}/g, elem.id],
				[/{name}/g, elem.name],
				[/{abl}/g, elem.id.toUpperCase()],
			];
			var textValues = [
				["abl_" + elem.id + "_mod", ablData.mod],
				["abl_" + elem.id + "_score", ablData.score]
			];
			addTemplateToTarget(abilityEntryTemp, abilityList, replacements, textValues, []);
		});
		setInputValue(formElem, "ability_notes", data.abilityNotes);
		setHidden(formElem, "ability_notes_block", isLocked, true, data.abilityNotes);
		// var ablNotesArray = formElem.querySelectorAll("#ability_notes_block");
		// if (ablNotesArray.length > 0) {
			// setClass(ablNotesArray[0], "hidden", !(isLocked) || (data.abilityNotes && data.abilityNotes.length > 0));
		// }		

		// proficiencies
		var profEntryTemp = formElem.querySelectorAll("#template_profentry")[0];
		var profList = formElem.querySelectorAll("#prof_list")[0];
		profList.innerHTML = "";
		data.proficiencies.forEach(function(elem) {
			var profInfo = infoDb.proficiencies.find(function(x) {return x.id == elem.id;});
			var clon = profEntryTemp.cloneNode(true);
			var clonHtml = clon.innerHTML.replace("{id}", elem.id);
			clonHtml = clonHtml.replace("{name}", profInfo.name);
			clonHtml = clonHtml.replace("{desc}", profInfo.desc);
			clon.innerHTML = clonHtml;
			setInputValue(clon.content.children[0], "prof_"+elem.id, elem.value);
			profList.appendChild(clon.content);
		});
	}, // buildForm
	
}

sections.push(sectionBasics);